describe("Tickets", () => {
  const customer = {
    firstName: "Lila",
    lastName: "Ricken",
    email: "lilianricken@gmail.com",
  };
  const firstname = "Lila";
  const lastname = "Ricken";
  const fullname = `${firstname} ${lastname}`;
  beforeEach(() =>
    cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
  );

  it("fills all the text fields", () => {
    cy.get("#first-name").type(firstname);
    cy.get("#last-name").type(lastname);
    cy.get("#email").type("lilianricken@gmail.com");
    cy.get("#requests").type("#PartiuPorto");
    cy.get("#signature").type("Lila no Porto");
  });
  it("select 2 tickets", () => {
    cy.get("#ticket-quantity").select("2");
  });
  it("select 'Vip' tickets", () => {
    cy.get("#vip").check();
  });
  it("select 'social media' checkbox", () => {
    cy.get("#social-media").check();
    cy.get("#friend").check();
    cy.get("#social-media").uncheck();
  });

  it("has 'TICKETBOX' header's heading", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });
  it("alerts on invalid email", () => {
    cy.get("#email").type("lilianricken-gmail.com");
    cy.get("#email.invalid").should("exist");
  });
  it("e2e", () => {
    cy.get("#first-name").type(firstname);
    cy.get("#last-name").type(lastname);
    cy.get("#email").type("lilianricken@gmail.com");
    cy.get("#requests").type("#PartiuPorto");
    cy.get("#signature").type("Lila no Porto");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get(".agreement p").should(
      "contain",
      `I, ${fullname}, wish to buy 2 VIP tickets.`
    );
    cy.get("#social-media").check();
    cy.get("#friend").check();
    cy.get("#agree").click();
    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");
    cy.get("button[type='reset']").click();
    cy.get("@submitButton").should("be.disabled");
  });

  it("função customizada", () => {
    cy.fillMandatoryFields(customer);
    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");
    cy.get("button[type='reset']").click();
    cy.get("@submitButton").should("be.disabled");
  });
});
